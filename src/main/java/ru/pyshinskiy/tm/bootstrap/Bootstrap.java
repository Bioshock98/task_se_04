package ru.pyshinskiy.tm.bootstrap;

import ru.pyshinskiy.tm.dao.repository.ProjectRepository;
import ru.pyshinskiy.tm.dao.repository.TaskRepository;
import ru.pyshinskiy.tm.dao.service.ProjectService;
import ru.pyshinskiy.tm.dao.service.TaskService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.erumerated.TerminalCommand;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.DateUtil.parseDateToString;

public class Bootstrap {

    private BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    private TaskRepository taskRepository = new TaskRepository();

    private ProjectRepository projectRepository = new ProjectRepository(taskRepository);

    private ProjectService projectService = new ProjectService(projectRepository);

    private TaskService taskService = new TaskService(taskRepository);

    public void start() {
        processConsole();
    }

    private void processConsole() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while(true) {
            try {
                String commandInput = input.readLine();
                TerminalCommand terminalCommand = TerminalCommand.valueOf(commandInput.toUpperCase());
                processCommand(terminalCommand);
                if(terminalCommand.equals(TerminalCommand.EXIT)) {
                    input.close();
                    System.exit(1);
                }
            }
            catch(IllegalArgumentException e) {
                System.out.println("UNKNOW COMMAND");
            }
            catch(IOException e) {
                System.out.println(e.toString());
            }
        }
    }

    private void processCommand(TerminalCommand terminalCommand) throws IOException {
        switch (terminalCommand) {
            case PROJECT_CLEAR:
                clearProject();
                break;
            case PROJECT_CREATE:
                createProject();
                break;
            case PROJECT_LIST:
                listProject();
                break;
            case PROJECT_SELECT:
                selectProject();
                break;
            case PROJECT_EDIT:
                editProject();
                break;
            case PROJECT_REMOVE:
                removeProject();
                break;
            case TASK_CLEAR:
                clearTask();
                break;
            case TASK_CREATE:
                createTask();
                break;
            case TASK_LIST:
                listTask();
                break;
            case TASK_LIST_BY_PROJECT:
                listTaskByProject();
                break;
            case TASK_SELECT:
                selectTask();
                break;
            case TASK_EDIT:
                editTask();
                break;
            case ATTACH_TASK:
                attachTask();
                break;
            case UNATTACH_TASK:
                unattachTask();
                break;
            case TASK_REMOVE:
                removeTask();
                break;
            case HELP:
                help();
                break;
        }
    }

    private void clearProject() {
        projectService.removeAll();
        System.out.println("[ALL PROJECT REMOVED]");
    }

    private void createProject() throws IOException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        Project project = new Project(input.readLine());
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        project.setEndDate(parseDateFromString(input.readLine()));
        projectService.persist(project);
        System.out.println("[OK]");
    }

    private void listProject() {
        System.out.println("[PROJECT LIST]");
        printProjects(projectService.findAll());
        System.out.println("[OK]");
    }

    private void selectProject() throws IOException {
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Project project = projectService.findOne(projectId);
        printProject(project);
    }

    private void editProject() throws IOException {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Project project = projectService.findOne(projectId);
        System.out.println("ENTER NAME");
        Project anotherProject = new Project(input.readLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        anotherProject.setEndDate(parseDateFromString(input.readLine()));
        projectService.merge(anotherProject);
        System.out.println("[OK]");
    }

    private void removeProject() throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        projectService.remove(projectId);
        System.out.println("[PROJECT REMOVED]");
    }

    private void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.removeAll();
        System.out.println("[ALL TASKS REMOVED]");
    }

    private void createTask() throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER TASK NAME]");
        Task task = new Task(input.readLine(), null);
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        task.setEndDate(parseDateFromString(input.readLine()));
        taskService.persist(task);
        System.out.println("[OK]");
        taskService.persist(task);
    }

    private void listTask() {
        System.out.println("[TASK LIST]");
        printTasks(taskService.findAll());
        System.out.println("[OK]");
    }

    private void listTaskByProject() throws IOException {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        List<Task> tasksByProjectId = taskService.findAllByProjectId(projectId);
        printTasks(tasksByProjectId);
    }

    private void selectTask() throws IOException {
        System.out.println("[TASK SELECT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Task task = taskService.findOne(taskId);
        printTask(task);
    }

    private void editTask() throws IOException {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Task task = taskService.findOne(taskId);
        System.out.println("[ENTER TASK NAME]");
        Task anotherTask = new Task(input.readLine(), null);
        anotherTask.setId(task.getId());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        anotherTask.setEndDate(parseDateFromString(input.readLine()));
        System.out.println("[OK]");
        taskService.merge(anotherTask);
    }

    private void attachTask() throws IOException {
        System.out.println("[ATTACH TASK]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        String projectId = projectService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Project project = projectService.findOne(projectId);
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        Task task = taskService.findOne(taskId);
        task.setProjectId(project.getId());
        System.out.println("[OK]");
    }

    private void unattachTask() throws IOException {
        System.out.println("[UNATTACH TASK]");
        printTasks(taskService.findAll());
        System.out.println("ENTER TASK ID");
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        taskService.findOne(taskId).setProjectId(null);
        System.out.println("[OK]");
    }

    private void removeTask() throws IOException {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        String taskId = taskService.getIdByNumber(Integer.parseInt(input.readLine()) - 1);
        taskService.remove(taskId);
        System.out.println("[TASK REMOVED]");
    }

    private void help() {
        StringBuilder help = new StringBuilder();
        help
                .append("help: Show all commands.\n")
                .append("project_clear: Remove all projects.\n")
                .append("project_create: Create new project.\n")
                .append("project_list: Show all projects.\n")
                .append("project_edit: Edit selected project\n")
                .append("project_remove: Remove selected project\n")
                .append("task_clear: Remove all tasks.\n")
                .append("task_create: Create new task.\n")
                .append("task_list: Show all tasks.\n")
                .append("task_edit: Edit selected task.\n")
                .append("task_remove: Remove selected task.");
        System.out.println(help);
    }

    private static void printProjects(Collection<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            final Project project = ((LinkedList<Project>)projects).get(i);
            System.out.println((i + 1) + "." + " " + project.getName());
        }
    }

    private static void printProject(Project project) {
        StringBuilder formatedProject = new StringBuilder();
        formatedProject.append("project name: ");
        formatedProject.append(project.getName());
        formatedProject.append("\nproject description: ");
        formatedProject.append(project.getDescription());
        formatedProject.append("\nstart date: ");
        formatedProject.append(parseDateToString(project.getStartDate()));
        formatedProject.append("\nend date: ");
        formatedProject.append(parseDateToString(project.getEndDate()));
        System.out.println(formatedProject);
    }

    private static void printTasks(List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + "." + " " + tasks.get(i).getName());
        }
    }

    private static void printTask(Task task) {
        StringBuilder formatedTask = new StringBuilder();
        formatedTask.append("task name: ");
        formatedTask.append(task.getName());
        formatedTask.append("\ntask description: ");
        formatedTask.append(task.getDescription());
        formatedTask.append("\nstart date: ");
        formatedTask.append(parseDateToString(task.getStartDate()));
        formatedTask.append("\nend date: ");
        formatedTask.append(parseDateToString(task.getEndDate()));
        System.out.println(formatedTask);
    }
}
