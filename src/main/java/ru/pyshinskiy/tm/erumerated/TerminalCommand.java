package ru.pyshinskiy.tm.erumerated;

public enum TerminalCommand {

    PROJECT_CLEAR,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_SELECT,
    PROJECT_EDIT,
    PROJECT_REMOVE,
    TASK_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_LIST_BY_PROJECT,
    TASK_SELECT,
    TASK_EDIT,
    ATTACH_TASK,
    UNATTACH_TASK,
    TASK_REMOVE,
    HELP,
    EXIT
}
