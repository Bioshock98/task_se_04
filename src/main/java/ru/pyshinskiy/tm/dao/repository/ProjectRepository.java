package ru.pyshinskiy.tm.dao.repository;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ProjectRepository extends AbstractRepository<Project> {

    private final Map<String, Project> projectMap = new LinkedHashMap<>();

    private final TaskRepository taskRepository;

    public ProjectRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Project> findAll() {
        return (LinkedList<Project>)projectMap.values();
    }

    @Override
    public Project findOne(String id) {
        return projectMap.get(id);
    }

    @Override
    public Project persist(Project project) {
        return projectMap.put(project.getId(), project);
    }

    @Override
    public Project merge(Project project) {
        Project updatingProject = findOne(project.getId());
        updatingProject.setName(project.getName());
        updatingProject.setDescription(project.getDescription());
        updatingProject.setStartDate(project.getStartDate());
        updatingProject.setEndDate(project.getEndDate());
        return projectMap.put(updatingProject.getId(), updatingProject);
    }

    @Override
    public Project remove(String id) {
        for(Task task : taskRepository.findAll()) {
            if(task.getProjectId().equals(id)) taskRepository.remove(task.getId());
        }
        return projectMap.remove(id);
    }

    @Override

    public void removeAll() {
        for(Task task : taskRepository.findAll()) {
            if(task.getProjectId() != null) taskRepository.remove(task.getId());
        }
        projectMap.clear();
    }
}
