package ru.pyshinskiy.tm.dao.service;

import ru.pyshinskiy.tm.dao.repository.ProjectRepository;
import ru.pyshinskiy.tm.dao.repository.TaskRepository;
import ru.pyshinskiy.tm.entity.AbstractEntity;
import ru.pyshinskiy.tm.entity.Project;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project findOne(String id) {
        return projectRepository.findOne(id);
    }

    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project persist(Project project) {
        return projectRepository.persist(project);
    }

    public Project merge(Project project) {
        return projectRepository.merge(project);
    }

    public Project remove(String id) {
        return projectRepository.remove(id);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public String getIdByNumber(int number) {
        List<Project> tasks = (LinkedList<Project>)findAll();
        for(Project project : findAll()) {
            if(project.getId().equals(tasks.get(number).getId())) return project.getId();
        }
        return null;
    }
}
