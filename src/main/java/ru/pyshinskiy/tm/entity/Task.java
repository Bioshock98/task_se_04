package ru.pyshinskiy.tm.entity;

import java.util.Date;

public class Task extends AbstractEntity {

    private String name;

    private String projectId;

    private String description;

    private Date startDate;

    private Date endDate;

    public Task() {
    }

    public Task(String name, String projectId) {
        super();
        this.name = name;
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public String setProjectId(String projectId) {
        return projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
